<?php

namespace judahnator\JsonManipulator\Tests;

use JsonException;
use judahnator\JsonManipulator\Json;
use PHPUnit\Framework\TestCase;

use function judahnator\JsonManipulator\load_json;

/**
 * Class JsonManipulatorTest
 * @package judahnator\JsonManipulator\Tests
 * @covers \judahnator\JsonManipulator\Json
 * @covers \judahnator\JsonManipulator\load_json()
 */
final class JsonManipulatorTest extends TestCase
{
    public function testCounting(): void
    {
        $object = '{"foo": "bar", "bing": ["baz"]}';
        $jsonObject = load_json($object);

        $array = '["one", "two", "three"]';
        $jsonArray = load_json($array);

        $this->assertEquals(2, count($jsonObject));
        $this->assertEquals(3, count($jsonArray));
    }

    public function testFetchingLiteral(): void
    {
        $json1 = load_json();
        $json1['foo'] = 'bar';

        $literal = $json1->getLiteral();

        $this->assertEquals('{"foo":"bar"}', $literal);
    }

    public function testLoadJsonFunction(): void
    {
        // Make sure no parameters return an empty object
        $jsonObject = load_json();
        $this->assertInstanceOf(Json::class, $jsonObject);
        $this->assertEquals('{}', (string)$jsonObject);

        // Test an array
        $jsonArray = '["one","two","three"]';
        $array = load_json($jsonArray);
        $this->assertInstanceOf(Json::class, $array);
        $this->assertEquals($jsonArray, (string)$array);
    }

    public function testMalformedJson(): void
    {
        $this->expectException(JsonException::class);
        $this->expectExceptionMessage('Syntax error');
        $malformedJson = 'invalid';
        load_json($malformedJson);
    }

    public function testBrokenJson(): void
    {
        $this->expectException(JsonException::class);
        $this->expectExceptionMessage('Syntax error');
        $brokenJson = '{"foo":["bar":"baz"]}';
        load_json($brokenJson);
    }

    public function testManipulatingObjects(): void
    {
        // Set up data
        $json = '{"foo":"bar"}';
        $jsonObject = load_json($json);

        // Sanity check
        $this->assertEquals('bar', $jsonObject['foo']);

        // Test changing values
        $jsonObject['foo'] = 'bing';
        $this->assertEquals('bing', $jsonObject['foo']);
        $this->assertTrue(isset($jsonObject['foo']));
        $this->assertEquals('{"foo":"bing"}', (string)$jsonObject);

        // Test unsetting values
        unset($jsonObject['foo']);
        $this->assertNull($jsonObject['foo']);
        $this->assertEquals('{}', $json);

        // Test adding objects
        $jsonObject['obj'] = ['one' => 'two'];
        $this->assertInstanceOf(Json::class, $jsonObject['obj']);
        $this->assertEquals('two', $jsonObject['obj']['one']);
        $this->assertEquals('{"obj":{"one":"two"}}', $json);
    }

    public function testManipulatingArrays(): void
    {
        // Set up data
        $json = '["zero","one","two"]';
        $jsonArray = load_json($json);

        // Sanity check
        $this->assertEquals('one', $jsonArray[1]);

        // Test adding values
        $jsonArray[] = '3';
        $this->assertEquals('3', $jsonArray[3]);
        $this->assertEquals('["zero","one","two","3"]', $json);

        // Test changing values
        $jsonArray[3] = 'three';
        $this->assertEquals('three', $jsonArray[3]);
        $this->assertEquals('["zero","one","two","three"]', $json);

        // Test unsetting values
        unset($jsonArray[2]);
        $this->assertEquals('three', $jsonArray[2]);
        $this->assertEquals('["zero","one","three"]', $json);

        // Test nesting values
        $jsonArray[0] = ['foo' => 'bar'];
        $this->assertInstanceOf(Json::class, $jsonArray[0]);
        $this->assertEquals('bar', $jsonArray[0]['foo']);
        $this->assertEquals('[{"foo":"bar"},"one","three"]', $json);
    }

    public function testObjectIteration(): void
    {
        $json = '{"foo": "bar", "bing": ["ching", "chong", "wing", "wong", "ting", "tong"], "baz": {"nested": "object"}}';
        $jsonObject = load_json($json);

        $index = 0;
        foreach ($jsonObject as $item => $value) {
            switch ($index) {
                case 0:
                    $this->assertEquals('foo', $item);
                    $this->assertEquals('bar', $value);
                    break;

                case 1:
                    $this->assertEquals('bing', $item);
                    $this->assertInstanceOf(Json::class, $value);
                    foreach ($value as $key => $nestedValue) {
                        $this->assertEquals($jsonObject['bing'][$key], $nestedValue);
                    }
                    break;

                case 2:
                    $this->assertEquals('baz', $item);
                    $this->assertInstanceOf(Json::class, $value);
                    $this->assertEquals('nested', $value->key());
                    $this->assertEquals('object', $value->current());
                    break;
            }
            $index++;
        }
    }

    public function testNestedElementManipulation(): void
    {
        $jsonString = '{"foo":["bar","baz"],"bing":{"asdf":"fdsa"}}';
        $jsonObject = load_json($jsonString);

        $jsonObject['foo'][] = 'bong';
        $jsonObject['bing']['asdf'] = 12345;

        $this->assertEquals(
            '{"foo":["bar","baz","bong"],"bing":{"asdf":12345}}',
            $jsonString
        );
    }

    public function testDebugInfo(): void
    {
        $arr = '["zero","one","two"]';
        $jsonArray = load_json($arr);

        $obj = '{"foo":"bar"}';
        $jsonObject = load_json($obj);

        $expectedArrayDebugInfo = <<<TEXT
judahnator\JsonManipulator\Json Object
(
    [0] => zero
    [1] => one
    [2] => two
)

TEXT;
        $expectedObjectDebugInfo = <<<TEXT
judahnator\JsonManipulator\Json Object
(
    [foo] => bar
)

TEXT;

        $this->assertEquals($expectedArrayDebugInfo, print_r($jsonArray, true));
        $this->assertEquals($expectedObjectDebugInfo, print_r($jsonObject, true));
    }
}
