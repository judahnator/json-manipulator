Changelog
=========

v4
--

Yet another complete rewrite. The idea here was to consolidate the logic down to a single class.

**Breaking changes:**
 * The minimum PHP version is now 8.1.
 * All object interaction is now done with brackets (`['foo']`) instead of arrows (`->foo`).
 * The `JsonArray` and `JsonObject` classes have been removed.
 * The `load_json()` method now throws a `JsonException` instead of an `InvalidArgumentException`.

v3.0.1
------

Fixing a bug where the library would not work with PHP 8.0 because some private methods were declared final, which no longer works.

v3
--

Version 3 is a complete rewrite. The idea was to implement much stronger typing, reduce the overall memory footprint, and remove internal usages of the `stdClass` class.

For the most part the API remains the same, there are just a few things to note around class naming and such.

**Breaking changes:**
 * The minimum PHP version is now 7.4.
 * The `JsonBase` class has been renamed to `Json`.
 * The `Json` class can no longer be invoked to update its string representation, this is now done automatically.
 * The `registerUpdateEventListener` method has been removed.
 * The `Json->setString()` function now throws a `JsonException` instead of an `InvalidArgumentException`.

v2
--

Version 2 is much the same as the first, in fact it is 100% compatible.
The main difference is it has a significantly smaller memory footprint by "bootstrapping" new `JsonBase` objects with initial data, instead of needing to make duplicate copies of subsets of your data.

v1
-

The first version of this library was more a proof of concept than anything else.