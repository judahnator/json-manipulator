<?php

declare(strict_types=1);

namespace judahnator\JsonManipulator;

use JsonException;

/**
 * Provided a JSON string, returns a JsonObject|JsonArray.
 *
 * @param string $json
 * @return Json
 * @throws JsonException
 */
function load_json(string &$json = '{}'): Json
{
    return new Json($json);
}
