<?php

declare(strict_types=1);

namespace judahnator\JsonManipulator;

use ArrayAccess;
use Countable;
use Iterator;
use JsonException;
use JsonSerializable;
use Stringable;

use function json_encode;

/**
 * @implements  ArrayAccess<int|string, Json|mixed>
 * @implements Iterator<int|string, Json|mixed>
 */
final class Json implements ArrayAccess, Countable, Iterator, JsonSerializable, Stringable
{
    /** @var array<int|string, Json|mixed> */
    private array $contents;

    private string $literal;

    /** @var callable|null */
    private $updateCallback;

    /**
     * @throws JsonException
     */
    public function __construct(string &$json = '{}', callable $updateCallback = null)
    {
        $this->contents = (array)json_decode($json, associative: true, flags: JSON_THROW_ON_ERROR);
        $this->literal = &$json;
        $this->updateCallback = $updateCallback;
    }

    /**
     * @return array<int|string, Json|mixed>|null
     */
    public function __debugInfo(): ?array
    {
        return $this->contents;
    }

    public function __toString(): string
    {
        return empty($this->contents) ? '{}' : (string)json_encode($this);
    }

    public function count(): int
    {
        return count($this->contents);
    }

    /**
     * @return Json|mixed|null
     */
    public function current(): mixed
    {
        return $this[$this->key()];
    }

    public function &getLiteral(): string
    {
        return $this->literal;
    }

    /**
     * @return array<int|string, Json|mixed>
     */
    public function jsonSerialize(): array
    {
        return $this->contents;
    }

    public function key(): int|string|null
    {
        return key($this->contents);
    }

    public function next(): void
    {
        next($this->contents);
    }

    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->contents);
    }

    public function offsetGet($offset): mixed
    {
        if (is_array($this->contents[$offset] ?? null)) {
            $json = new self(updateCallback: [$this, 'update']);
            $json->setContents($this->contents[$offset]);
            $this->contents[$offset] = $json;
        }
        return $this->contents[$offset] ?? null;
    }

    public function offsetSet($offset, $value): void
    {
        if ($offset === null) {
            $this->contents[] = $value;
        } else {
            $this->contents[$offset] = $value;
        }
        $this->update();
    }

    public function offsetUnset($offset): void
    {
        // Checks to see if this is a list
        $reindex = is_int($offset) && array_keys($this->contents) === range(0, count($this->contents) - 1);
        unset($this->contents[$offset]);
        if ($reindex) {
            $this->contents = array_values($this->contents);
        }
        $this->update();
    }

    public function rewind(): void
    {
        reset($this->contents);
    }

    /**
     * @param array<int|string, Json|mixed> $contents
     * @return void
     */
    public function setContents(array $contents): void
    {
        $this->contents = $contents;
        $this->update();
    }

    private function update(): void
    {
        $this->literal = (string)$this;
        if (is_callable($this->updateCallback)) {
            ($this->updateCallback)();
        }
    }

    public function valid(): bool
    {
        return ($key = $this->key()) !== null && array_key_exists($key, $this->contents);
    }
}
