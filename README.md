Json Manipulator
================

[![pipeline status](https://gitlab.com/judahnator/json-manipulator/badges/master/pipeline.svg)](https://gitlab.com/judahnator/json-manipulator/commits/master)
[![coverage report](https://gitlab.com/judahnator/json-manipulator/badges/master/coverage.svg)](https://gitlab.com/judahnator/json-manipulator/-/commits/master) 


What is this?
-------------

The short version is it's a library that allows you to interact with JSON strings in a more natural way, as if they were native PHP variables.

Usage
-----

The main class you will be using is `\judahnator\JsonManipulator\Json`.

It is set up automatically when using the factory function, `\judahnator\JsonManipulator\load_json()`.

For example:
```php
<?php

use function judahnator\JsonManipulator\load_json;

$jsonString = '{"foo":"bar"}';
load_json($jsonString)['foo'] = 'baz';

echo $jsonString; 
// '{"foo":"baz"}'
```

You may also work with nested properties.
```php
<?php

use function judahnator\JsonManipulator\load_json;

$jsonString = '{"foo":["bar","baz"]}';
$jsonObject = load_json($jsonString);
$jsonObject['foo'][] = 'bong';

echo $jsonString; 
// '{"foo":["bar","baz", "bong"]}'
```

You can also add/remove objects and arrays freely.

```php
<?php

use function judahnator\JsonManipulator\load_json;

$jsonObject = load_json();
$jsonObject['object'] = ['foo' => 'bar'];
$jsonObject['array'] = ['zero', 'one', 'two'];

echo $jsonObject; 
// {"object":{"foo":"bar"},"array":["zero","one","two"]}
```